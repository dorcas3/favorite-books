# Favorite Books

# Project description

A simple application that will demonstrate sharing data across different components.

## Live link

https://pedantic-pasteur-986845.netlify.app/


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
