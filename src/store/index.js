
import { createStore } from 'vuex'

export const store = createStore({
    state: {
        books: [
            {
                id: "0",
                name: "Leadership and Self Deception ",
                author: "Arbinger Institute",
                description: "The book’s central insight–that the key to leadership lies not in what we do but in who we are–has proven to have powerful implications not only for organizational leadership but in readers’ personal lives as well. "

            }
        ]

    },
    getters: {
        getBooks: state => state.books
    },
    mutations: {
        addBook: (state, payload) => {
            const newBook = {
                id: payload.id,
                name: payload.name,
                author: payload.author,
                description: payload.description
            };
            state.books.unshift(newBook);
        },
        deleteBook: (state, payload) => {
            const index = state.books.findIndex(book => book.id === payload);
            state.books.splice(index, 1);
        },

    },
    actions: {
        addBookAction: (context, payload) => {
            context.commit("addBook", payload);
        },
        deleteBookAction: (context, payload) => {
            context.commit("deleteBook", payload);
        },

    }
})